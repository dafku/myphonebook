package domain;

import data.DataProvider;
import data.Contact;

public class MyPhoneBook {
    private ObservableQueue observableQueue;

    public MyPhoneBook(String filePath) {
        observableQueue = ObservableQueue.getInstance();
        observableQueue.init(filePath);
    }

    public void add(String name, String phoneNumber, String type) {
        observableQueue.add(new Contact(name, phoneNumber, type));
    }

    public void add(Contact contact) {
        observableQueue.add(contact);
    }

    public void remove(int id) {
        observableQueue.remove(id);
    }

    public void remove(Contact contact) {
        observableQueue.remove(contact.getId());

    }

    public void edit(Contact toEdit, String name, String phoneNumber, String type) {
        observableQueue.edit(new Contact(name, phoneNumber, type), toEdit.getId());
    }

    public void edit(Integer toEdit, String name, String phoneNumber, String type) {
        observableQueue.edit(new Contact(name, phoneNumber, type), toEdit);
    }

    public void edit(Contact toEdit, Contact secondary) {
        observableQueue.edit(secondary, toEdit.getId());
    }

    public void edit(Integer toEdit, Contact secondary) {
        observableQueue.edit(secondary, toEdit);
    }

    public void find(String text, DataProvider dataProvider) {
        observableQueue.find(text, dataProvider);
    }

    public void getList(DataProvider dataProvider) {
        observableQueue.getList(dataProvider);
    }
}
