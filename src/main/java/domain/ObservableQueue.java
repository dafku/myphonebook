package domain;

import data.DataProvider;
import data.Contact;
import data.ListPayload;
import data.Payload;
import io.reactivex.BackpressureStrategy;
import io.reactivex.Flowable;
import io.reactivex.subjects.PublishSubject;
import io.reactivex.subjects.Subject;
import io.reactivex.subscribers.ResourceSubscriber;

public class ObservableQueue<T> {
    public static final int INSERT = 0;
    public static final int SEARCH = 1;
    public static final int ALL = 3;
    public static final int EDIT = 2;
    private final Subject<T> subject = PublishSubject.create();
    private DataHandler dataHandler;
    private static ObservableQueue observableQueue;

    private ObservableQueue() {
        dataHandler = new DataHandler();
        startListening();
    }

    public static ObservableQueue getInstance() {
        if (observableQueue == null) ;
        observableQueue = new ObservableQueue();
        return observableQueue;
    }

    private Flowable<T> observe() {
        if (!subject.hasObservers()) {
            return subject
                    .toFlowable(BackpressureStrategy.BUFFER);
        }
        throw new UnsupportedOperationException("Already has 1 observer");
    }

    private void startListening() {
        observe()
                .subscribe(new ResourceSubscriber() {
                    @Override
                    public void onNext(Object o) {
                        if (o instanceof Payload) {
                            doWithPayload((Payload) o);
                        } else if (o instanceof String) {
                            dataHandler.init((String) o);
                        } else if (o instanceof Integer) {
                            dataHandler.remove((Integer) o);
                        } else if (o instanceof ListPayload) {
                            dataHandler.getList((ListPayload) o);
                        }
                    }

                    @Override
                    public void onError(Throwable t) {
                        System.out.println(t.getMessage());
                    }

                    @Override
                    public void onComplete() {
                        System.out.println("d");
                    }
                })
        ;
    }

    private void doWithPayload(Payload o) {
        switch (o.getType()) {
            case INSERT:
                dataHandler.add(((Payload) o).getContact());
                break;
            case EDIT:
                dataHandler.edit(((Payload) o).getContact());
                break;

        }
    }

    public void add(Contact contact) {
        Payload payload = new Payload();
        payload.setContact(contact);
        payload.setType(INSERT);
        subject.onNext((T) payload);
    }

    public void init(String filePath) {
        subject.onNext((T) filePath);
    }

    public void remove(Integer id) {
        subject.onNext((T) id);
    }

    public void find(String text, DataProvider dataProvider) {
        ListPayload listPayload = new ListPayload();
        listPayload.setText(text);
        listPayload.setDataProvider(dataProvider);
        listPayload.setType(SEARCH);
        subject.onNext((T) listPayload);
    }

    public void getList(DataProvider dataProvider) {
        ListPayload listPayload = new ListPayload();
        listPayload.setDataProvider(dataProvider);
        listPayload.setType(ALL);
        subject.onNext((T) listPayload);
    }

    public void edit(Contact secondary, Integer primaryId) {
        secondary.setId(primaryId);
        Payload payload = new Payload();
        payload.setContact(secondary);
        payload.setType(EDIT);
        subject.onNext((T) payload);
    }

}
