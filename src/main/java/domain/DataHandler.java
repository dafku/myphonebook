package domain;

import com.google.gson.Gson;
import data.Contact;
import data.ContactList;
import data.ListPayload;
import io.reactivex.Observable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Function;
import io.reactivex.functions.Predicate;
import io.reactivex.schedulers.Schedulers;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class DataHandler {
    Disposable dataInserter;
    private List<Contact> contacts;
    Gson gson;
    private int maxId = 0;

    public DataHandler() {
        gson = new Gson();
    }

    public void add(Contact contact) {
        System.out.println(Thread.currentThread().getId() + Thread.currentThread().getName() + " " + contact.getName());
        contact.setId(++maxId);
        contacts.add(contact);
        sortList();
        saveList();
    }

    public void saveList() {
        if (dataInserter != null && !dataInserter.isDisposed())
            dataInserter.dispose();
        saveDisposable();
    }

    private void saveDisposable() {
        dataInserter = Observable.just(contacts)
                .map(new Function<List<Contact>, List<Contact>>() {
                    @Override
                    public List<Contact> apply(List<Contact> contacts) throws Exception {
                        ArrayList<Contact> contacts1 = new ArrayList<>();
                        for (int i = 0; i < contacts.size(); i++)
                            contacts1.add(contacts.get(i));
                        return contacts1;
                    }
                })
                .subscribeOn(Schedulers.io())
                .observeOn(Schedulers.io())
                .map(new Function<List<Contact>, String>() {
                    @Override
                    public String apply(List<Contact> contacts) throws Exception {
                        Gson gson = new Gson();
                        ContactList contactList = new ContactList();
                        contactList.setContacts(contacts);
                        return gson.toJson(contactList);
                    }
                })
                .map(new Function<String, String>() {
                    @Override
                    public String apply(String s) throws Exception {
                        FileUtils.writeStringToFile(file, s);
                        return s;
                    }
                })
                .subscribe();
    }

    private void sortList() {
        Collections.sort(contacts, new Comparator<Contact>() {
            @Override
            public int compare(Contact o1, Contact o2) {
                return o1.getName().compareTo(o2.getName());
            }
        });

    }

    File file;

    public void init(String fileName) {
        contacts = Collections.synchronizedList(new ArrayList<Contact>());
        System.out.println(Thread.currentThread().getId() + Thread.currentThread().getName() + " " + " " + " " + fileName);

        file = new File(fileName);
        try {
            if (!file.exists()) {
                file.createNewFile();
            } else {
                Gson gson = new Gson();
                String a = FileUtils.readFileToString(file);
                ContactList contacts = gson.fromJson(String.valueOf(a), ContactList.class);
                getMaxId(contacts.getContacts());
                this.contacts = contacts.getContacts();
            }

        } catch (Exception e) {

        }
    }

    private void getMaxId(List<Contact> contacts) {
        for (Contact contact : contacts) {
            if (contact.getId() > maxId)
                maxId = contact.getId();
        }
    }

    public void remove(Integer o) {
        Observable.fromIterable(contacts)
                .filter(new Predicate<Contact>() {
                    @Override
                    public boolean test(Contact contact) throws Exception {
                        if (contact.getId() == o)
                            return false;
                        else return true;
                    }
                })
                .toList()
                .toObservable()
                .map(new Function<List<Contact>, Boolean>() {
                    @Override
                    public Boolean apply(List<Contact> contacts) throws Exception {
                        DataHandler.this.contacts = contacts;
                        saveList();
                        return true;
                    }
                })
                .subscribe();
    }

    public void edit(Contact contact) {
        Observable.fromIterable(contacts)
                .map(new Function<Contact, Contact>() {
                    @Override
                    public Contact apply(Contact contactt) throws Exception {
                        if (contact.getId() == contactt.getId())
                            return contact;
                        return contactt;
                    }
                })
                .toList()
                .toObservable()
                .map(new Function<List<Contact>, Boolean>() {
                    @Override
                    public Boolean apply(List<Contact> contacts) throws Exception {
                        DataHandler.this.contacts = contacts;
                        sortList();
                        saveList();
                        return true;
                    }
                })
                .subscribe();
    }

    public void getList(ListPayload listPayload) {
        Observable.fromIterable(contacts)
                .filter(new Predicate<Contact>() {
                    @Override
                    public boolean test(Contact contact) throws Exception {
                        if (listPayload.getType() == ObservableQueue.SEARCH) {
                            if (contact.getName().contains(listPayload.getText()) || ((Integer) contact.getId()).toString().contains(listPayload.getText()) || contact.getPhoneNumber().contains(listPayload.getText()) || contact.getType().contains(listPayload.getText())) {
                                return true;
                            }
                            return false;
                        } else {
                            return true;
                        }
                    }
                })
                .toList()
                .toObservable()
                .map(new Function<List<Contact>, Boolean>() {
                    @Override
                    public Boolean apply(List<Contact> contacts) throws Exception {
                        listPayload.getDataProvider().contacts(contacts);
                        return true;
                    }
                }).subscribe();
    }
}
