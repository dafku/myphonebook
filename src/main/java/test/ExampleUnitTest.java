package test;

import data.Contact;
import data.DataProvider;
import domain.MyPhoneBook;
import io.reactivex.Observable;
import io.reactivex.functions.Function;
import io.reactivex.functions.Function5;
import io.reactivex.schedulers.Schedulers;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import java.io.File;
import java.util.List;
import java.util.Optional;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertEquals;

@RunWith(MockitoJUnitRunner.class)
public class ExampleUnitTest {
    private String filename = "testfile.txt";

    @Test
    public void insert() {
        removeFile();
        MyPhoneBook myPhoneBook = new MyPhoneBook(filename);
        Observable.just(myPhoneBook)
                .map(new Function<MyPhoneBook, MyPhoneBook>() {
                    @Override
                    public MyPhoneBook apply(MyPhoneBook myPhoneBook) throws Exception {
                        myPhoneBook.add("a", "b", "c");
                        return myPhoneBook;
                    }
                })
                .delay(10, TimeUnit.MILLISECONDS)
                .map(new Function<MyPhoneBook, MyPhoneBook>() {
                    @Override
                    public MyPhoneBook apply(MyPhoneBook myPhoneBook) throws Exception {
                        myPhoneBook.getList(new DataProvider() {
                            @Override
                            public void contacts(List<Contact> contacts) {
                                assertEquals(contacts.size(), 1);
                                assertEquals(contacts.get(0).getName(), "a");
                                assertEquals(contacts.get(0).getPhoneNumber(), "b");
                                assertEquals(contacts.get(0).getType(), "c");
                            }
                        });
                        return myPhoneBook;
                    }
                })
                .subscribe();
        sleep(20);
    }

    @Test
    public void remove() {
        insert();
        MyPhoneBook phoneBook = new MyPhoneBook(filename);
        phoneBook.remove(1);
        phoneBook.getList(new DataProvider() {
            @Override
            public void contacts(List<Contact> contacts) {
                assertEquals(contacts.size(), 0);
            }
        });
        sleep(10);
    }

    @Test
    public void search() {
        removeFile();
        MyPhoneBook myPhoneBook = new MyPhoneBook(filename);
        Observable.just(myPhoneBook)
                .map(new Function<MyPhoneBook, MyPhoneBook>() {
                    @Override
                    public MyPhoneBook apply(MyPhoneBook myPhoneBook) throws Exception {
                        myPhoneBook.add("richard", "b", "c");
                        myPhoneBook.add("fd", "06985522", "c");
                        myPhoneBook.add("fff", "dr", "test");
                        return myPhoneBook;
                    }
                })
                .delay(30, TimeUnit.MILLISECONDS)
                .map(new Function<MyPhoneBook, MyPhoneBook>() {
                    @Override
                    public MyPhoneBook apply(MyPhoneBook myPhoneBook) throws Exception {
                        myPhoneBook.find("richard", new DataProvider() {
                            @Override
                            public void contacts(List<Contact> contacts) {
                                assertEquals(contacts.size(), 1);
                                assertEquals(contacts.get(0).getName(), "richard");
                            }
                        });
                        myPhoneBook.find("06985522", new DataProvider() {
                            @Override
                            public void contacts(List<Contact> contacts) {
                                assertEquals(contacts.size(), 1);
                                assertEquals(contacts.get(0).getPhoneNumber(), "06985522");
                            }
                        });
                        myPhoneBook.find("test", new DataProvider() {
                            @Override
                            public void contacts(List<Contact> contacts) {
                                assertEquals(contacts.size(), 1);
                                assertEquals(contacts.get(0).getType(), "test");
                            }
                        });
                        return myPhoneBook;
                    }
                })
                .delay(30, TimeUnit.MILLISECONDS)
                .subscribe();
        sleep(100);
    }

    @Test
    public void filter() {
        search();
        MyPhoneBook myPhoneBook = new MyPhoneBook(filename);
        myPhoneBook.getList(new DataProvider() {
            @Override
            public void contacts(List<Contact> contacts) {
                assertEquals(contacts.get(0).getName(), "fd");
                assertEquals(contacts.get(1).getName(), "fff");
                assertEquals(contacts.get(2).getName(), "richard");
            }
        });
        sleep(10);
    }

    @Test
    public void multiThreading() {
        sleep(500);
        removeFile();
        MyPhoneBook myPhoneBook = new MyPhoneBook(filename);
        sleep(200);
        Observable
                .zip(bruteforceInsert(myPhoneBook), bruteforceInsert(myPhoneBook), bruteforceInsert(myPhoneBook), bruteforceInsert(myPhoneBook), bruteforceInsert(myPhoneBook), new Function5<Integer, Integer, Integer, Integer, Integer, Integer>() {
                    @Override
                    public Integer apply(Integer integer, Integer integer2, Integer integer3, Integer integer4, Integer integer5) throws Exception {
                        return integer + integer2 + integer3 + integer4 + integer5;
                    }
                })
                .delay(5000, TimeUnit.MILLISECONDS)
                .map(new Function<Integer, Integer>() {
                    @Override
                    public Integer apply(Integer integer) throws Exception {
                        myPhoneBook.getList(new DataProvider() {
                            @Override
                            public void contacts(List<Contact> contacts) {
                                assertEquals(contacts.size(), Optional.ofNullable(integer));
                            }
                        });
                        return integer;
                    }
                })
                .subscribe();
        sleep(1000);
    }

    private Observable<Integer> bruteforceInsert(MyPhoneBook myPhoneBook) {
        Random a = new Random();
        int nr = a.nextInt(100);

        return Observable.range(0, nr)
                .observeOn(Schedulers.io())
                .map(new Function<Integer, Integer>() {
                    @Override
                    public Integer apply(Integer integer) throws Exception {
                        myPhoneBook.add(integer.toString(), integer.toString(), integer.toString());
                        return integer;
                    }
                })
                .lastElement()
                .toObservable()
                .map(new Function<Integer, Integer>() {
                    @Override
                    public Integer apply(Integer integer) throws Exception {
                        return ++integer;
                    }
                });

    }

    private void removeFile() {
        File file = new File(filename);
        file.delete();

    }

    private void sleep(int a) {
        try {
            Thread.sleep(a);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
