package data;

import java.util.List;

public interface DataProvider {
    void contacts(List<Contact> contacts);
}
